<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./css/top.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム画面</title>

</head>
<body>
    <div class="header" align="center">
    <c:if test="${ empty loginUser }">
        <a href="login">ログイン</a>
    </c:if>
    <c:if test="${ not empty loginUser }">
        <a href="./">ホーム</a>
        <c:if test="${loginUser.department_id == 1}"><a href="management">ユーザー管理</a></c:if>
        <a href= "newpost">投稿</a>
        <a href="logout">ログアウト</a>
    </c:if>
    <br />
<form action="./" method="get">
	<input type="text" name="category"  placeholder="キーワードをどうぞ" value="${keep}"><c:remove var="keep" scope="session"/><br />
	<input type="date" name="start" value="${keepstart}"><c:remove var="keepstart" scope="session"/>～
	<input type="date" name="end" value="${keepend}"><c:remove var="keepend" scope="session"/>
	<input type="submit"  value="クリック">
</form>

    <br /><c:out value="ログイン中のユーザー： ${loginUser.name} さん" /><br />
        
                <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
				</c:if> 
				 
<div class="toppage">
  <c:forEach items="${posts}" var="post"><br />
   <div class="posts">
      <input type="hidden" name="post_id" value="${post.id}" />
       <span class="subject">【件名】<br /><c:out value="${post.subject}" /></span><br />
       <p class="text">【本文】<br />
         <c:forEach var="str" items="${ fn:split(post.text,'
') }">
            			${str}<br/>
		 </c:forEach></p><br />
       <span class="category">【カテゴリー】<br /><c:out value="${post.category}" /></span><br />                    
       <span class="name">投稿者<c:out value="${post.name}" /></span><br>
       <span class="date"><fmt:formatDate value="${post.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></span>


      <c:if test="${loginUser.id == post.user_id}">
        <form action="postdelete" method="post">
        	<input type="hidden" name="post_id" value="${post.id}"/><br>
        	<input type="submit" value="投稿削除" onClick="return disp();">
        </form>
      </c:if><br /> 
   </div>
   
   <div class="comments">
      <c:forEach items="${comments}" var="comment">
         <c:if test="${post.id == comment.post_id}"><br />
            
            <p class="commenttext">（コメント）<br/>
            <c:forEach var="str" items="${ fn:split(comment.text,'
') }">
            ${str}<br/>
            </c:forEach></p><br/>
            
            <span class="name">（コメントした人）<br />
            <c:out value="${comment.name}"/></span><br>
            
            <span class="date">
            <fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />
            </span>

			<c:if test="${loginUser.id == comment.user_id}">
            	<form action="commentdelete" method="post">
            		<input type="hidden" name="comment_id" value="${comment.id}" />
		            <input type="submit" value="コメント削除" onClick="return disp();" />
        	    </form>
            </c:if>
         </c:if>
     </c:forEach>
   </div>
                        
    <form action="comment" method="post">          
    		<input type="hidden" name="post_id" value="${post.id}" />
            <div class="comment">コメント<br />
            
            <textarea name="comment" cols="30" ><c:if test="${post.id == democomment.post_id}">${democomment.text}<c:remove var="democomment" scope="session"/></c:if></textarea>
            
            <br /><input type="submit" value="投稿" /><br />
            
            </div>
            <br />
    </form>
    </c:forEach>
</div>
<div class="copyright"> Copyright(c) i a .</div>

<script type="text/javascript">
<!--
function disp(){
	if(window.confirm('削除しますか？')){
		return true;
    }else{
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false;
	}
}
// -->
</script>
</div>
</body>
</html>

