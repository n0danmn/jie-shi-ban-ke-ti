<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>投稿画面</title>
</head>
<body>
<div class="form-area">

           <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>
            
	<form action="newpost" method="post">
	    件名<br />
	    <input type="text" name="subject"  style="width:400px;" class="tweet-box"  value="${demopost.subject}" />
	    <br />
	    本文
	    <br />
	    <textarea name="text" cols="55" rows="10" class="tweet-box" ><c:out value="${demopost.text}" /></textarea>
	    <br />
	    カテゴリー
	    <br />
	    <input type="text" name="category"  style="width:400px;" class="tweet-box"  value="${demopost.category}" />
	    <br />
	    <input type="submit" value="投稿">
	    <br />
	    <br />
	    <a href="./">戻る</a><br /><br />
	</form>
	<br />
</div>
</body>
</html>
