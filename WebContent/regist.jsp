<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <link href=".css/style.css" rel="stylesheet" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー新規登録</title>
    </head>
    
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
     
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="regist" method="post"><br />
                
                <label for="name">名前</label> <input name="name" value="${demoregist.name}" id="name" /> <br /> 
                                
                <label for="login_id">ログインID</label> <input name="login_id" value="${demoregist.login_id}" id="login_id" /><br />
                
                <label for="password">パスワード</label> <input name="password" type="password" value="" id="password" /> <br /> 
                
                <label for="password2">確認用パスワード</label> <input name="password2" type="password" value="" id="password" /> <br />
                                
                <label for="branch_id">支店名</label>
                <select name="branch_id">
                <option value="0">選択してください</option>
                <c:forEach items="${branches }" var="branch">
                <c:if test="${demoregist.branch_id == branch.id}">
				<option value="${branch.id }" selected>${branch.name}</option>
				</c:if>
				<c:if test="${demoregist.branch_id != branch.id}">
				<option value="${branch.id }">${branch.name}</option>
				</c:if>
				</c:forEach>
				</select><br />
                
                <label for ="department_id">部署・役職</label>
                <select name="department_id">
                <option value="0">選択してください</option>
                <c:forEach items="${departments }" var="department">
                <c:if test="${demoregist.department_id == department.id}">
				<option value="${department.id }" selected>${department.name}</option>
				</c:if>
				<c:if test="${demoregist.department_id != department.id}">
				<option value="${department.id }">${department.name}</option>
				</c:if>
				</c:forEach>
				</select><br />
                    
                <br /> <input type="submit" value="新規登録" /> <br /> <a href="management">戻る</a>
                <c:remove var="demoregist" scope="session" />
            </form>
            <div class="copyright">Copyright(c)Iwahori Arisa</div>
        </div>
    </body>
</html>