<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>

    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>
            
            
            <form action="usermanagement" method="post">
            <br />
            <input type="hidden" name="id" value="${editUser.id}" />
            <input type="hidden" name="before_id" value="${before_id}" />

                <label for="name">名前</label> <input name="name" value="${editUser.name}"id="name" /> <br /> 
            
                <label for="login_id">ログインID</label> <input name="login_id" value="${editUser.login_id}"id="login_id" /><br />
                
                <label for="password">パスワード</label> <input name="password" type="password" value="" id="password" /> <br /> 
                
                <label for="password2">確認用パスワード</label> <input name="password2" type="password" value="" id="password" /> <br />
                
                <label for="branch_id">支店名</label>
                
                <%-- ログインしている場合 --%>
                <c:if test="${loginUser.id == editUser.id}">
                  <input type="hidden" name="branch_id" value="${editUser.branch_id}">
                  <select disabled>
                  <c:forEach items="${branches }" var="branch">
				  <c:if test="${editUser.branch_id == branch.id}">
				    <option value="${branch.id }" selected><c:out value="${branch.name}" ></c:out>
				  </c:if>
                  </c:forEach>
                  </select>
                </c:if>
                
                <%-- ログインしていない場合 --%>
                <c:if test="${loginUser.id != editUser.id}">
                <select name="branch_id" >
                <option value="0">選択してください</option>
                <c:forEach items="${branches }" var="branch">
                <c:if test="${editUser.branch_id == branch.id}">
				<option value="${branch.id }" selected>${branch.name}</option>
				</c:if>
				<c:if test="${editUser.branch_id != branch.id}">
				<option value="${branch.id }">${branch.name}</option>
				</c:if>
				</c:forEach>
				</select>
				</c:if>
				<br />
                
                <label for ="department_id">部署・役職</label>
                
                <c:if test="${loginUser.id == editUser.id}">
                <input type="hidden" name="branch_id" value="${editUser.branch_id}">
                <select disabled>
                <option value="0">選択してください</option>
                <c:forEach items="${departments }" var="department">
                <c:if test="${editUser.department_id == department.id}">
				<option value="${department.id }" selected>${department.name}</option>
				</c:if>
				<c:if test="${editUser.department_id != department.id}">
				<option value="${department.id }">${department.name}</option>
				</c:if>
				</c:forEach>
				</select>
				</c:if>
								
			    <c:if test="${loginUser.id != editUser.id}">
                <select name="department_id" >
                <option value="0">選択してください</option>
                <c:forEach items="${departments }" var="department">
                <c:if test="${editUser.department_id == department.id}">
				<option value="${department.id }" selected>${department.name}</option>
				</c:if>
				<c:if test="${editUser.department_id != department.id}">
				<option value="${department.id }">${department.name}</option>
				</c:if>
				</c:forEach>
				</select>
				</c:if>
				<br />
                    
                <%-- <input name="branch_id" value="${editUser.branch_id}"id="branch_id" /><br />
                <input name="department_id" value="${editUser.department_id}" id="department_id"><br /> --%>
            
                 <input type="submit" value="編集登録" /> <br />
                <a href="./management">戻る</a>
            <%-- </c:forEach> --%></form>
            <div class="copyright"> Copyright(c)Iwahori Arisa</div>
        </div>
    </body>