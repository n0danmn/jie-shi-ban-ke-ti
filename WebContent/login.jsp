<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	 <link href="./css/login.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン</title>
</head>

<body>
	<div class="main-contents">
	<p style="color:black; font-weight: bolder; font-size: 8ex; font-family:Impact; ">Bulletin board</p>

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
             <c:forEach items="${errorMessages}" var="message">
				<c:out value="${message}" />
             </c:forEach>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="login" method="post"><p>
				<input name="login_id" placeholder="ログインID" value="${miss}"/><br><br>
				<c:remove var="miss" scope="session"/>
				<input name="password" type="password" id="password" placeholder="パスワード" /><br><br>

                <input type="submit" class="loginbutton" value="ログイン" /><br />
                
            </p></form>
		<p class="copyright">Copyright(c)Iwahori</p>
	</div>
</body>
</html>