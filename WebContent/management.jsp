<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<body>
	<a href="regist">新規登録する</a><br />	
	<a href="./">戻る</a><br />
	
    <c:if test="${ not empty errorMessages }">
       <div class="errorMessages">
           <ul>
               <c:forEach items="${errorMessages}" var="message">
                   <li><c:out value="${message}" />
               </c:forEach>
           </ul>
       </div>
       <c:remove var="errorMessages" scope="session"/>
   </c:if>
	
	<div class="user">
	<table>
	  
	<tr>
    <th>名前</th>
    <th>ログインID</th>
    <th>支店名</th>
    <th>部署・役職名</th>
    <th>ログイン状況</th>
    <th>編集</th>
    </tr>
    <c:forEach items="${users}" var="user">
    <tr>
    <td><c:out value="${user.name}" /></td>
    <td><c:out value="${user.login_id}" /></td>
    <td><c:forEach items="${branches}" var="branch">
				   <c:if test="${branch.id == user.branch_id}">
		   				<c:out value="${branch.name}" />
				   </c:if>
				   </c:forEach>
				   </td>
    <td><c:forEach items="${departments}" var="department">
					<c:if test="${department.id == user.department_id}">
				   		<c:out value="${department.name}" />
				   	</c:if>
				   </c:forEach>
				   </td>
    <td><c:choose>
				   <c:when test="${loginUser.id == user.id}">
				   	ログイン中 </c:when>
				   <c:otherwise>
				   <form action ="isstopped" method="post">
				   <c:if test="${user.is_stopped == 0}">
				    <p><input type="submit" value="停止" onClick="return disp();"></p>
				   <!-- <input type="submit" value="停止"> -->
				   <input type="hidden" name="user_id" value="${user.id }" />
				   <input type="hidden" name="status" value="1" />
				   </c:if>
				   
				   <c:if test="${user.is_stopped == 1}">
				    <p><input type="submit" value="復活" onClick="return disp2();"></p>
				   <!-- <input type="submit" value="復活"> -->
				   <input type="hidden" name="user_id" value="${user.id }" />
				   <input type="hidden" name="status" value="0" />
				   </c:if></form>
				   </c:otherwise></c:choose><br/></td>
				   
    <td><a href="usermanagement?id=${user.id}">編集</a></td>
    </tr>
    </c:forEach>
    </table>
	</div>
			<div class="copyright"> Copyright(c)Iwahori Arisa</div>
</body>
<script type="text/javascript">
<!--
function disp(){
	if(window.confirm('ユーザーを停止しますか？')){
		return true;
    }else{
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false;
	}
}
// -->
<!--
function disp2(){
	if(window.confirm('ユーザーを復活させますか？')){
		return true;
    }else{
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false;
	}
}
// -->
</script>
</html>

