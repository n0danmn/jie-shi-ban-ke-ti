package iwahori_arisa.service;

import static iwahori_arisa.utils.CloseableUtil.*;
import static iwahori_arisa.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import iwahori_arisa.beans.Comment;
import iwahori_arisa.beans.UserComment;
import iwahori_arisa.dao.CommentDao;
import iwahori_arisa.dao.UserCommentDao;

public class CommentService {
	private static final int LIMIT_NUM = 1000;
	

    public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
            
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public List<UserComment> getUserComments() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserCommentDao usercommentDao = new UserCommentDao();
            List<UserComment> ret = usercommentDao.getUserComments(connection,LIMIT_NUM);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public void delete (int id){
    	Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.delete(connection, id);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
