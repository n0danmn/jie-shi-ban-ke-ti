package iwahori_arisa.service;

import static iwahori_arisa.utils.CloseableUtil.*;
import static iwahori_arisa.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import iwahori_arisa.beans.Post;
import iwahori_arisa.beans.UserPost;
import iwahori_arisa.dao.PostDao;
import iwahori_arisa.dao.UserPostDao;

public class PostService {
	private static final int LIMIT_NUM = 1000;
	

    public void register(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.insert(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public List<UserPost> getUserPost(String category, String start, String end) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserPostDao postDao = new UserPostDao();
            List<UserPost> ret = postDao.getUserMessages(connection,LIMIT_NUM, category, start, end);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public void delete(int id) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.delete(connection, id);

            commit(connection);
            return;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
        
    }
    

}