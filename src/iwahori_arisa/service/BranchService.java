package iwahori_arisa.service;

import static iwahori_arisa.utils.CloseableUtil.*;
import static iwahori_arisa.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import iwahori_arisa.beans.Branch;
import iwahori_arisa.dao.BranchDao;

public class BranchService {

	 public List<Branch> getBranches() {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            BranchDao branchDao = new BranchDao();
	            List<Branch> ret = branchDao.getBranches(connection);

	            commit(connection);
	            return ret;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
}
