package iwahori_arisa.service;

import static iwahori_arisa.utils.CloseableUtil.*;
import static iwahori_arisa.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import iwahori_arisa.beans.User;
import iwahori_arisa.dao.UserDao;
import iwahori_arisa.utils.CipherUtil;

public class UserService {
	private static final int LIMIT_NUM = 1000;

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public List<User> getUser() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            List<User> ret = userDao.getUserManagement(connection,LIMIT_NUM);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public User getUser(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User user = userDao.getUser(connection, userId);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public void  update (User user){

        Connection connection = null;
        try {
            connection = getConnection();

            if (!StringUtils.isNullOrEmpty(user.getPassword())){
	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);
            }
            
            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }	 
    }
    
    public void move(User move){
        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.move(connection, move);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
    public User getLoginid(String login_id) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User user = userDao.getLogin_id(connection, login_id);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
}