package iwahori_arisa.service;

import static iwahori_arisa.utils.CloseableUtil.*;
import static iwahori_arisa.utils.DBUtil.*;

import java.sql.Connection;

import iwahori_arisa.beans.User;
import iwahori_arisa.dao.UserDao;
import iwahori_arisa.utils.CipherUtil;

public class LoginService {

    public User login(String login_id, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, login_id, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}