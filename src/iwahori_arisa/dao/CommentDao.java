package iwahori_arisa.dao;

import static iwahori_arisa.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import iwahori_arisa.beans.Comment;
import iwahori_arisa.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("  post_id");
            sql.append(", text");
            sql.append(", user_id");  
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("  ?"); //post_id
            sql.append(", ?"); //text
            sql.append(", ?"); //user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            
            ps.setInt(1, comment.getPost_id());
            ps.setString(2, comment.getText());
            ps.setInt(3, comment.getUser_id());
         
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    
    public void delete (Connection connection, int id) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM comments ");
            sql.append("WHERE id = ? ");
            
            
            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, id);
            
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}