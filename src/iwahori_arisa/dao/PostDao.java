package iwahori_arisa.dao;

import static iwahori_arisa.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import iwahori_arisa.beans.Post;
import iwahori_arisa.exception.SQLRuntimeException;

public class PostDao {

    public void insert(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO posts ( ");
            sql.append("  subject");
            sql.append(", text");
            sql.append(", category");            
            sql.append(", created_date");
            sql.append(", user_id");            
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("  ?"); //subject
            sql.append(", ?"); //text
            sql.append(", ?"); //category
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", ?");//user_id
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, post.getSubject());
            ps.setString(2, post.getText());
            ps.setString(3, post.getCategory());
            ps.setInt(4, post.getUser_id());
            

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    
    public void delete (Connection connection, int id) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM posts ");
            sql.append("WHERE id = ? ");
            
            
            ps = connection.prepareStatement(sql.toString());
            
            ps.setInt(1, id);
            
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}
