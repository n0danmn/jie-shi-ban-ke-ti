package iwahori_arisa.dao;
import static iwahori_arisa.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import iwahori_arisa.beans.User;
import iwahori_arisa.exception.NoRowsUpdatedRuntimeException;
import iwahori_arisa.exception.SQLRuntimeException;



public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", is_stopped");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // department_id
            sql.append(", ?"); // is_stopped
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getDepartment_id());
            ps.setInt(6, user.getIs_stopped());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	
	public User getUser(Connection connection, String login_id,
        String password) {

    PreparedStatement ps = null;
    try {
        String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? "
        		+ "AND is_stopped = 0 ";

        ps = connection.prepareStatement(sql);
        ps.setString(1, login_id);
        ps.setString(2, password);
        
        ResultSet rs = ps.executeQuery();
        List<User> userList = toUserList(rs);
        if (userList.isEmpty() == true) {
            return null;
        } else if (2 <= userList.size()) {
            throw new IllegalStateException("2 <= userList.size()");
        } else {
            return userList.get(0);
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}
	
	private List<User> toUserList(ResultSet rs) throws SQLException {

    List<User> ret = new ArrayList<User>();
    try {
        while (rs.next()) {
            int id = rs.getInt("id");
            String login_id = rs.getString("login_id");
            String password = rs.getString("password");
            String name = rs.getString("name");
            Integer branch_id = rs.getInt("branch_id");
            Integer department_id = rs.getInt("department_id");
            Integer is_stopped = rs.getInt("Is_stopped");
            Timestamp created_date = rs.getTimestamp("created_date");
            Timestamp updated_date = rs.getTimestamp("updated_date");

            User user = new User();
            user.setId(id);
            user.setLogin_id(login_id);
            user.setPassword(password);
            user.setName(name);
            user.setBranch_id(branch_id);
            user.setDepartment_id(department_id);
            user.setIs_stopped(is_stopped);
            user.setCreated_date(created_date);
            user.setUpdated_date(updated_date);

            ret.add(user);
        }
        return ret;
    } finally {
        close(rs);
    }
    }
	
	public List<User> getUserManagement (Connection connection, int num) {

    PreparedStatement ps = null;
    try {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        sql.append(" id");
        sql.append(",login_id");
        sql.append(",password");
        sql.append(",name");
        sql.append(",branch_id");
        sql.append(",department_id");
        sql.append(",is_stopped");
        sql.append(" FROM users");
        //sql.append("ORDER BY created_date DESC limit " + num);

        ps = connection.prepareStatement(sql.toString());

        ResultSet rs = ps.executeQuery();
        List<User> ret = toUserList1(rs);
        return ret;
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
    }
	
	private List<User> toUserList1(ResultSet rs)
        throws SQLException {

    List<User> ret = new ArrayList<User>();
    try {
        while (rs.next()) {
            Integer id = rs.getInt("id");
            String login_id = rs.getString("login_id");
            String password = rs.getString("password");
            String name = rs.getString("name");
            Integer branch_id = rs.getInt("branch_id");
            Integer department_id = rs.getInt("department_id");
            Integer is_stopped = rs.getInt("is_stopped");
            //Timestamp createdDate = rs.getTimestamp("created_date");

            User user = new User();
            user.setId(id);
            user.setLogin_id(login_id);
            user.setPassword(password);
            user.setName(name);
            user.setBranch_id(branch_id);
            user.setDepartment_id(department_id);
            user.setIs_stopped(is_stopped);
            //user.setCreated_date(createdDate);

            ret.add(user);
        }
        return ret;
    } finally {
        close(rs);
    }
	}
	
	public void update(Connection connection, User user) {

	    PreparedStatement ps = null;
	    try {
	        StringBuilder sql = new StringBuilder();
	        sql.append("UPDATE users SET ");
	        sql.append("login_id = ? ");
	        sql.append(", name = ? ");
	        sql.append(", branch_id = ? ");
	        sql.append(", department_id = ? ");
	        if (!StringUtils.isNullOrEmpty(user.getPassword())){
	        	sql.append(", password = ? ");
	        }
	        sql.append(", updated_date = CURRENT_TIMESTAMP");
	        sql.append(" WHERE ");
	        sql.append(" id = ? ");


	        ps = connection.prepareStatement(sql.toString());
	        
	        ps.setString(1, user.getLogin_id());
	        ps.setString(2, user.getName());
	        ps.setInt(3, user.getBranch_id());
	        ps.setInt(4, user.getDepartment_id());
	        if (!StringUtils.isNullOrEmpty(user.getPassword())){
		        ps.setString(5, user.getPassword());
		        ps.setInt(6, user.getId());
	        } else {
	        	ps.setInt(5, user.getId());
	        }
	        
	        
	        int count = ps.executeUpdate();
	        if ( count == 0){
	        	throw new NoRowsUpdatedRuntimeException();
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	        }
	    }

	public User getUser(Connection connection, int userId) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE id = ? ";

	        ps = connection.prepareStatement(sql);
	        ps.setInt(1, userId);
	        
	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
	
	public void move(Connection connection,User move){
		 
		PreparedStatement ps = null;
		    try {
		        StringBuilder sql = new StringBuilder();
		        sql.append("UPDATE users SET ");
		        sql.append("is_stopped = ? ");
		        sql.append("WHERE id = ?");
		        
		        ps = connection.prepareStatement(sql.toString());
		        
		        ps.setInt(1, move.getIs_stopped());
		        ps.setInt(2, move.getId());
		        
		        ps.executeUpdate();
		    }catch (SQLException e) {
		        throw new SQLRuntimeException(e);
		    } finally {
		        close(ps);
		        }
	}
	public User getLogin_id(Connection connection, String login_id) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE login_id = ? " ;

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, login_id);
	        
	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
}

