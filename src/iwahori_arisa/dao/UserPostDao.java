package iwahori_arisa.dao;

import static iwahori_arisa.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import iwahori_arisa.beans.UserPost;
import iwahori_arisa.exception.SQLRuntimeException;

public class UserPostDao {

    public List<UserPost> getUserMessages(Connection connection, int num, 
    		String category, String start, String end) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.subject as subject,");
            sql.append("posts.text as text,");
            sql.append("posts.category as category,");
            sql.append("posts.id as id,  ");
            sql.append("users.name as name,  ");
            sql.append("posts.user_id as user_id, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM users ");
            sql.append("INNER JOIN posts ");
            sql.append("ON users.id = posts.user_id ");
            sql.append("WHERE ");
            sql.append("posts.created_date >= ? "); //start
            sql.append("AND posts.created_date <= ? "); //end
            if (!StringUtils.isEmpty(category) && !StringUtils.isBlank(category)) {
            	sql.append("AND category LIKE ? ");
        	}
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, start);
            ps.setString(2, end);
            if (!StringUtils.isEmpty(category) && !StringUtils.isBlank(category)) {
            	ps.setString(3, "%" + category + "%");
            }

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);
            return ret;
            
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserPost> toUserPostList(ResultSet rs) throws SQLException {

        List<UserPost> ret = new ArrayList<UserPost>();
        try {
            while (rs.next()) {
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Integer id = rs.getInt("id");
                String name = rs.getString("name");
                Integer user_id = rs.getInt("user_id");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserPost post = new UserPost();
                post.setSubject(subject);
                post.setText(text);
                post.setCategory(category);
                post.setPost_id(id);
                post.setName(name);
                post.setUser_id(user_id);
                post.setCreated_date(createdDate);

                ret.add(post);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
        
}
