package iwahori_arisa.dao;

import static iwahori_arisa.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import iwahori_arisa.beans.UserComment;
import iwahori_arisa.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.text as text,");
            sql.append("users.name as name,");
            sql.append("comments.created_date as created_date, ");
            sql.append("comments.post_id as post_id, ");
            sql.append("comments.id as id, ");
            sql.append("comments.user_id as user_id ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs) throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                String text = rs.getString("text");
                String name = rs.getString("name");
                Timestamp createdDate = rs.getTimestamp("created_date");
                int post_id = rs.getInt("post_id");
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");

                UserComment comment = new UserComment();
                comment.setText(text);
                comment.setName(name);
                comment.setCreated_date(createdDate);
                comment.setPost_id(post_id);
                comment.setId(id);
                comment.setUser_id(user_id);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}
