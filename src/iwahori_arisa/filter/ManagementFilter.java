package iwahori_arisa.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import iwahori_arisa.beans.User;

@WebFilter(urlPatterns = { "/management" , "/usermanagement" , "/regist" })
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		HttpSession session = ((HttpServletRequest)request).getSession();
		String path = ((HttpServletRequest)request).getServletPath();
		List<String> messages = new ArrayList<String>();
		
		if (" /management , /usermanagement , /regist".equals(path)) {
			chain.doFilter(request, response);
			return;
		}
		
		User loginUser = (User) session.getAttribute("loginUser");
		
		if (loginUser == null) {
			chain.doFilter(request, response);
			return;
		}
		
		if(loginUser.getBranch_id() != 1 ){
			messages.add("指定したページにあなたはアクセス権限がありません");
            session.setAttribute("errorMessages", messages);
			((HttpServletResponse) response).sendRedirect("./");
			return;
		}
		if(loginUser.getDepartment_id() != 1 ){
			messages.add("指定したページにあなたはアクセス権限がありません");
            session.setAttribute("errorMessages", messages);
			((HttpServletResponse) response).sendRedirect("./");
			return;
		}
		chain.doFilter(request, response);		
	}
	
	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}
}