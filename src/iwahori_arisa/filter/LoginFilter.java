package iwahori_arisa.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import iwahori_arisa.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		String path = ((HttpServletRequest)request).getServletPath();
		List<String> messages = new ArrayList<String>();
		
		//servlet
		if ("/login".equals(path) || path.endsWith(".css")) {
			chain.doFilter(request, response);
			return;
		}
		
		if (session == null) {
			((HttpServletResponse) response).sendRedirect("./login");
			messages.add("ログインをしてください");
//            session.setAttribute("errorMessages", messages);
			return;
		}
		
		User loginUser = (User) session.getAttribute("loginUser");
		
		if (loginUser == null) {
			((HttpServletResponse) response).sendRedirect("./login");
			messages.add("ログインをしてください");
            session.setAttribute("errorMessages", messages);
			return;
		}
		chain.doFilter(request, response); // サーブレットを実行

	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}
