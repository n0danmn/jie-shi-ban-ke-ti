package iwahori_arisa.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import iwahori_arisa.beans.User;
import iwahori_arisa.service.UserService;

@WebServlet(urlPatterns = { "/isstopped" })
public class IsstoppedServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	
    	User move = new User();
    	move.setIs_stopped(Integer.parseInt(request.getParameter("status")));   	
    	move.setId(Integer.parseInt(request.getParameter("user_id")));

        new UserService().move(move);
        
        response.sendRedirect("./management");
    }
}
