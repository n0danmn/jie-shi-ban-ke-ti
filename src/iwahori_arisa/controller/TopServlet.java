
package iwahori_arisa.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import iwahori_arisa.beans.UserComment;
import iwahori_arisa.beans.UserPost;
import iwahori_arisa.service.CommentService;
import iwahori_arisa.service.PostService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	
    	HttpSession session = request.getSession();
    	String category = request.getParameter("category");
    	String start = request.getParameter("start");
    	String end = request.getParameter("end");
    	
    	session.setAttribute("keep", category);
    	session.setAttribute("keepend", end);
    	session.setAttribute("keepstart", start);
    	
    	
    	
    	// 入力されていない場合はデフォルトの値で検索
    	if (StringUtils.isEmpty(start) || StringUtils.isBlank(start)) {
    		start = "2018-01-01";
    	}
    	if (StringUtils.isEmpty(end) || StringUtils.isBlank(end)) {
    		end = "2118-12-31";
    	}
    	end = end + " 23:59:59";
    	
    	List<UserPost> posts = new PostService().getUserPost(category, start, end);
    	request.setAttribute("posts", posts);
    	
    	List<UserComment> comments = new CommentService().getUserComments();
    	request.setAttribute("comments", comments);
	
    	request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
    
}
