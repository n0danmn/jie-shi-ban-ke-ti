package iwahori_arisa.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import iwahori_arisa.beans.Branch;
import iwahori_arisa.beans.Department;
import iwahori_arisa.beans.User;
import iwahori_arisa.service.BranchService;
import iwahori_arisa.service.DepartmentService;
import iwahori_arisa.service.UserService;

@WebServlet(urlPatterns = { "/regist" })
public class RegistServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();
		List<Branch> branches = new BranchService().getBranches();
		session.setAttribute("branches", branches);
		
		List<Department> departments = new DepartmentService().getDepartments();
		session.setAttribute("departments",departments);
        request.getRequestDispatcher("regist.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        User regist= getRegist(request);
        
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
            user.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));

            
            new UserService().register(user);

            response.sendRedirect("./management");
        } else {
            session.setAttribute("errorMessages", messages);
            session.setAttribute("demoregist", regist);
            response.sendRedirect("regist");
        }
    }
    
    private User getRegist(HttpServletRequest request)
            throws IOException, ServletException {
    	 
    	User regist = new User();
    	
        regist.setLogin_id(request.getParameter("login_id"));
        regist.setPassword(request.getParameter("password"));
        regist.setName(request.getParameter("name"));
        regist.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        regist.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));

        return regist;
       
    }    
    

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");
        String branch_id = request.getParameter("branch_id");
        String department_id = request.getParameter("department_id");


        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }else{
        if (10 < name.length()) {
            messages.add("名前は10文字以下で入力してください");
        }
        }
        
        if (StringUtils.isBlank(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }else{
	        if (login_id.length() < 6 || login_id.length() > 20) {
	            messages.add("ログインIDは6文字以上20文字以下で入力してください");
	        }
	        if(!login_id.matches("^[A-Za-z0-9]+$")){
	        	messages.add("ログインIDは半角英数字で入力してください");
	        }
    		User login = new UserService().getLoginid(login_id);
    		if (login != null){
    			messages.add("そのログインIDは使用できません");
    		}
        }
        
        if (StringUtils.isBlank(password) == true) {
        	messages.add("パスワードを入力してください");
        } else {
	        if (password.length() < 6 || password.length() > 20) {
	            messages.add("パスワードは6文字以上20文字以下で入力してください");
	        }
	        if(!password.matches("^[A-Za-z0-9!-/:-@¥[-`{-~]]+$")){
	        	messages.add("パスワードは半角英数字で入力してください");
	        }
	        if (Objects.equals(password, password2) != true) {
	            messages.add("確認用パスワードが違います");
	        }
        }
        if (Integer.parseInt(branch_id) == 0) {
            messages.add("支店名を選択してください");
        }
        if (Integer.parseInt(department_id) == 0) {
            messages.add("部署・役職名を選択してください");
        }            
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}