package iwahori_arisa.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import iwahori_arisa.service.PostService;

@WebServlet(urlPatterns = { "/postdelete" })
public class PostDeleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	
    	
    	int post_id = Integer.parseInt(request.getParameter("post_id"));    	
        new PostService().delete(post_id);
        
        response.sendRedirect("./");
    }
}
