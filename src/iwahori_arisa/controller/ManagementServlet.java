package iwahori_arisa.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import iwahori_arisa.beans.Branch;
import iwahori_arisa.beans.Department;
import iwahori_arisa.beans.User;
import iwahori_arisa.service.BranchService;
import iwahori_arisa.service.DepartmentService;
import iwahori_arisa.service.UserService;

@WebServlet(urlPatterns = {"/management"})
	public class ManagementServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;
		
		@Override
		protected void doGet(HttpServletRequest request,
				HttpServletResponse response) throws IOException, ServletException {
			
			List<User> user = new UserService().getUser();			
			request.setAttribute("users", user);
			
			List<Branch> branches = new BranchService().getBranches();
			request.setAttribute("branches", branches);
			
			List<Department> departments = new DepartmentService().getDepartments();
			request.setAttribute("departments",departments);
			
			
			request.getRequestDispatcher("/management.jsp").forward(request, response);
		}  

}

