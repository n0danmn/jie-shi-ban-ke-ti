package iwahori_arisa.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import iwahori_arisa.beans.Branch;
import iwahori_arisa.beans.Department;
import iwahori_arisa.beans.User;
import iwahori_arisa.exception.NoRowsUpdatedRuntimeException;
import iwahori_arisa.service.BranchService;
import iwahori_arisa.service.DepartmentService;
import iwahori_arisa.service.UserService;

@WebServlet(urlPatterns = { "/usermanagement" })
public class SettingServlet  extends HttpServlet{
	//private static final long seriaVersionUID = 1L;
	
	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws  ServletException,IOException {
		
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		
		if(request.getParameter("id").length() == 0 || !request.getParameter("id").matches("^[0-9]*$")){
			messages.add("不正なパラメーターです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}
		int user_id = Integer.parseInt(request.getParameter("id"));
		

		User editUser = new UserService().getUser(user_id);
		if(editUser == null){
			messages.add("不正なパラメーターです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}
		
		request.setAttribute("editUser", editUser);
		
    	
		List<Branch> branches = new BranchService().getBranches();
		session.setAttribute("branches", branches);
		
		List<Department> departments = new DepartmentService().getDepartments();
		session.setAttribute("departments",departments);
				
		session.setAttribute("before_id", editUser.getLogin_id());
		
    	request.getRequestDispatcher("/setting.jsp").forward(request, response);
    	}

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {
            try {          	
                new UserService().update(editUser);
                
            } catch (NoRowsUpdatedRuntimeException e) {            	
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("setting.jsp").forward(request, response);
                return;
            }
            response.sendRedirect("./management");
            
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.getRequestDispatcher("setting.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {
    	 
    	User editUser = new User();
    	
    	
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setLogin_id(request.getParameter("login_id"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setName(request.getParameter("name"));
        editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        editUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));

        return editUser;
       
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {	  	
    	
    	
    	String name = request.getParameter("name");
    	String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String branch_id = request.getParameter("branch_id");
        String department_id = request.getParameter("department_id");

		String before_id = request.getParameter("before_id");

        
        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        } else {
        	if (10 < name.length()) {
            messages.add("名前は10文字以下で入力してください");
            }
        }
        
        if (StringUtils.isBlank(login_id) == true) {
            messages.add("ログインIDを入力してください");
        } else {      
	        if (!before_id.equals(login_id)){
	    		User login = new UserService().getLoginid(login_id);    		
	    		if (login != null){
	    			messages.add("そのログインIDは使用できません");
	    		}
	    	}
	        if (login_id.length() < 6 || login_id.length() > 20) {
	            messages.add("ログインIDは6文字以上20文字以下で入力してください");
	        }
	        if(!login_id.matches("^[A-Za-z0-9]+$")){
	        	messages.add("ログインIDは半角英数字で入力してください");
	        }
	    }
       
        if (StringUtils.isBlank(password) != true) {
            if (password.length() < 6 || password.length() > 20) {
                messages.add("パスワードは6文字以上20文字以下で入力してください");
            }
            if(!password.matches("^[A-Za-z0-9!-/:-@¥[-`{-~]]+$")){
            	messages.add("パスワードは半角英数字で入力してください");
            }           
	        if (Objects.equals(password, password2) != true) {
	            messages.add("確認用パスワードが違います");
	        }
        }
        
        if (Integer.parseInt(branch_id) == 0) {
            messages.add("支店名を選択してください");
        }
        if (Integer.parseInt(department_id) == 0) {
            messages.add("部署・役職名を選択してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}