package iwahori_arisa.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import iwahori_arisa.beans.Comment;
import iwahori_arisa.beans.User;
import iwahori_arisa.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	
    	    	
        request.getRequestDispatcher("./").forward(request, response);
        
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> messages = new ArrayList<String>();
        Comment comment1 = getNewCommment(request);
        
        

        if (isValid(request, messages) == true) {
        	
            User user = (User) session.getAttribute("loginUser");

            Comment comment = new Comment();
            comment.setPost_id(Integer.parseInt(request.getParameter("post_id")));
            comment.setText(request.getParameter("comment"));
            comment.setUser_id(user.getId());
                        
            new CommentService().register(comment);

            response.sendRedirect("./");
            
        } else {
            session.setAttribute("errorMessages", messages);
            session.setAttribute("democomment",comment1 );
            
            //request.getRequestDispatcher("./").forward(request, response);
            response.sendRedirect("./");
            
        }
    }
    
    private Comment getNewCommment (HttpServletRequest request)
            throws IOException, ServletException {
    	 
    	Comment comments = new Comment();
    	
        comments.setText(request.getParameter("comment"));
        comments.setPost_id(Integer.parseInt(request.getParameter("post_id")));
        
        return comments;
       
    }

	private boolean isValid(HttpServletRequest request, List<String> messages) {

        String comment = request.getParameter("comment");

        if (StringUtils.isBlank(comment) == true) {
            messages.add("コメントを入力してください");
        }
        if (500 < comment.length()) {
            messages.add("500文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        }else {
            return false;
        }
    }

}
